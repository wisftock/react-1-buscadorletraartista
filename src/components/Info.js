import React from 'react';

const Info = ({ infomation }) => {
  if (Object.keys(infomation).length === 0) return null;
  const { strArtistThumb, strBiographyES, strGenre } = infomation;
  return (
    <div className='card boder-light'>
      <div className='card-header bg-primary text-whtite font-weight-bold text-center'>
        Información Artista
      </div>
      <div className='card-body'>
        <img src={strArtistThumb} alt='Logo artista' />
        <p className='card-text'>Género: {strGenre}</p>
        <h2 className='card-text'>Biografía:</h2>
        <p className='card-text'>{strBiographyES}</p>
      </div>
    </div>
  );
};

export default Info;

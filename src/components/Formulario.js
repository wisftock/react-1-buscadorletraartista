import React, { useState } from 'react';
import Error from './Error';

const Formulario = ({ setSearchLetra }) => {
  const [search, setSearch] = useState({
    artista: '',
    cancion: '',
  });
  const [error, setError] = useState(false);
  const { artista, cancion } = search;
  const handleOnChange = (e) => {
    setSearch({ ...search, [e.target.name]: e.target.value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (artista.trim() === '' || cancion.trim() === '') {
      setError(true);
      return;
    }
    setError(false);
    setSearchLetra(search);
  };
  return (
    <div className='bg-info'>
      {error && <Error mensaje='Todos los campos son obligatorios' />}
      <div className='container'>
        <div className='row'>
          <form
            className='col card text-white bg-transparent mb-3 pt-3'
            onSubmit={handleSubmit}
          >
            <fieldset>
              <legend className='text-center'>Buscador Letras Canciones</legend>
              <div className='row'>
                <div className='col-md-6'>
                  <div className='form-group'>
                    <label>Artista</label>
                    <input
                      type='text'
                      value={artista}
                      name='artista'
                      className='form-control'
                      placeholder='Nombre Artista'
                      onChange={handleOnChange}
                    />
                  </div>
                </div>
                <div className='col-md-6'>
                  <div className='form-group'>
                    <label>Canción</label>
                    <input
                      type='text'
                      value={cancion}
                      name='cancion'
                      className='form-control'
                      placeholder='Nombre Canción'
                      onChange={handleOnChange}
                    />
                  </div>
                </div>
              </div>
              <button className='btn btn-success float-right'>Buscar</button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Formulario;

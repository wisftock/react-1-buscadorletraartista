import Formulario from './components/Formulario';
import { Fragment, useEffect, useState } from 'react';
import axios from 'axios';
import Cancion from './components/Cancion';
import Info from './components/Info';

function App() {
  const [searchLetra, setSearchLetra] = useState({});
  const [letras, setLetras] = useState('');
  const [infomation, setInfomation] = useState({});
  useEffect(() => {
    if (Object.keys(searchLetra).length === 0) return;

    const consultarAPIS = async () => {
      const { artista, cancion } = searchLetra;
      const url = `https://api.lyrics.ovh/v1/${artista}/${cancion}`;
      const url2 = `https://www.theaudiodb.com/api/v1/json/1/search.php?s=${artista}`;

      const [letra, informacion] = await Promise.all([axios(url), axios(url2)]);

      setLetras(letra.data.lyrics);
      setInfomation(informacion.data.artists[0]);
    };
    consultarAPIS();
  }, [searchLetra]);
  return (
    <Fragment>
      <Formulario setSearchLetra={setSearchLetra} />
      <div className='container mt-5'>
        <div className='row'>
          <div className='col-md-6'>
            <Info infomation={infomation} />
          </div>
          <div className='col-md-6'>
            <Cancion letras={letras} />
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
